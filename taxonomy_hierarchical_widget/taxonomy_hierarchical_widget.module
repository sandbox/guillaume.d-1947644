<?php
/**
* @file
* New taxonomy reference widget
*/

/*********************/
/** Administration  **/
/*********************/

/**
 * Implements hook_field_widget_settings_form().
 */
function taxonomy_hierarchical_widget_field_widget_settings_form( $field, $instance ) {
  $widget = $instance['widget'];
  $settings = $widget['settings'] + field_info_widget_settings( $widget['type'] );
  
  // Control settings
  if (!isset($settings['multiple_choice']))
    $settings['multiple_choice'] = FALSE;
  
  $form = array();
  if ($widget['type'] == 'taxonomy_hierarchical_select') {
    $form['multiple_choice'] = array(
      '#type' => 'checkbox',
      '#title' => t('Multiple choice'),
      '#default_value' => $settings['multiple_choice'],
      '#description' => t('Check this box if user can select multiple references.'),
    );
  }
  return $form;
}


/*********************/
/**    Content      **/
/*********************/

/**
 * Implement hook_init().
 */
function taxonomy_hierarchical_widget_init() {
  
  //Add css
  drupal_add_css(drupal_get_path('module', 'taxonomy_hierarchical_widget') . '/css/taxonomy_hierarchical_widget.css');
}

/**
 * Implements hook_field_widget_info().
 */
function taxonomy_hierarchical_widget_field_widget_info() {
  $widgets['taxonomy_hierarchical_select'] = array(
    'label' => t('Hierarchical select'),
    'description' => t('Taxonomy hierarchical select'),
    'field types' => array('taxonomy_term_reference'),
  );
  return $widgets;
}

/**
 * Implements hook_field_widget_form().
 */
function taxonomy_hierarchical_widget_field_widget_form( &$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element ) {
  global $user;
  
  // Control widget type
  if ($instance['widget']['type'] == 'taxonomy_hierarchical_select') {
        
    // Delete add more button if choice is not multiple
    if (!isset($instance['widget']['settings']['multiple_choice']) || !$instance['widget']['settings']['multiple_choice'])
      $form['#after_build'][] = 'taxonomy_hierarchical_widget_widget_after_build';
    
    // Add validate method
    $element['#validate'][] = 'taxonomy_hierarchical_widget_element_validate';
    
    // Build widget
    if (isset($field['settings']['allowed_values'][0]['vocabulary'])) { // Get vocabulary from field settings
      $vocabulary = taxonomy_hierarchical_widget_get_vocabulary_by_name( $field['settings']['allowed_values'][0]['vocabulary'] );
      if (!is_null($vocabulary)) {
        $taxonomy_tree = taxonomy_get_tree( $vocabulary->vid ); 
        $taxonomy_tree_max_depth = taxonomy_hierarchical_widget_get_max_depth( $taxonomy_tree );
        $options = taxonomy_hierarchical_widget_get_taxonomy_options( $taxonomy_tree, $taxonomy_tree_max_depth );
        $field_name = $element['#field_name'];
        $field_language = $element['#language'];
        
        // Workbench access tree of current user
        $workbench_acces_tree = workbench_access_get_user_tree( $user );
          
        // Delete default values if user has not permission
        if (isset($items) && !isset($form['#node']) || !isset($form['#node']->nid)) {
          foreach ($items as $item_key => $item) {
            if (!isset($workbench_acces_tree[$item['tid']]))
              unset($items[$item_key]);
          }
        }

        // Get taxonomies (from database and from form)
        (isset($items[$delta]['tid'])) ? $item_tid = $items[$delta]['tid'] : $item_tid = NULL;
        (isset($form_state['values'][$field_name][$field_language][$delta]['tid'])) ? $form_tid = $form_state['values'][$field_name][$field_language][$delta]['tid'] : $form_tid = NULL; 
        if (!isset($form_tid) && !isset($form['#node']->nid)) { // Parameters by url (default value)
          $query_parameters = drupal_get_query_parameters();
          if (isset($query_parameters['tid']) && isset($workbench_acces_tree[$query_parameters['tid']])) {
            $term = taxonomy_term_load($query_parameters['tid']);
            if ($term->vid == $vocabulary->vid)
              $form_tid = $term->tid;
          }
        }
        $item_taxonomy = taxonomy_hierarchical_widget_get_term_instance( $taxonomy_tree, $item_tid );
        $form_taxonomy = taxonomy_hierarchical_widget_get_term_instance( $taxonomy_tree, $form_tid );
        (isset($item_taxonomy) && !isset($workbench_acces_tree[$item_taxonomy->tid])) ? $element_attributes = array('disabled' => 'disabled') : $element_attributes = NULL;
        $element_disabled = FALSE;
        
        // Builds selects
        for ($i=0; $i<=$taxonomy_tree_max_depth; $i++) {
          
          // Get options and default value
          if (isset($element_attributes)) { // Edition with a term without user's permission
            if ($item_taxonomy->depth == $i) {
              $element_options = array( $item_taxonomy->tid => $item_taxonomy->name );
              $default_value = $item_taxonomy->tid;
            }
            else{ 
              if ($item_taxonomy->depth < $i) { // Edition with a term with inferior deep
                $element_options = array('_none' => t('- None -'));
                $default_value = '_none';
              }
              else{ // Edition with a term with superior depth
                $depth_parent_taxonomy = taxonomy_hierarchical_widget_get_depth_parent( $taxonomy_tree, $item_taxonomy->tid, $i );
                $element_options = array( $depth_parent_taxonomy->tid => $depth_parent_taxonomy->name );
                $default_value = $depth_parent_taxonomy->tid;
              }
            }
          }
          else{  // Creation or ajax callback
            $parent = $i-1;
            if (isset($element['depth_' . $parent]['#default_value'])) {
              $parent_value = $element['depth_' . $parent]['#default_value'];
              $element_options = taxonomy_hierarchical_widget_options_by_parent( $parent_value );
            }
            else{
              $element_options = $options[$i];
            }
            
            if (isset($form_taxonomy))
              $taxonomy_id = $form_taxonomy->tid;
            elseif (isset($item_taxonomy))
              $taxonomy_id = $item_taxonomy->tid;
            else
              $taxonomy_id = NULL;
            $default_value = taxonomy_hierarchical_widget_get_default_value( $taxonomy_tree, $element_options, $i, $taxonomy_id );
          }
          
          // Don't add select if there is only one options
          if (count($element_options) == 1 && isset($element_options['_none']))
            break;
          
          $element['depth_' . $i] = array(
            '#type' => 'select',
            '#attributes' => array('class' => array('taxonomy_hierarchical_input_select')),
            '#default_value' => $default_value,
            '#options' => $element_options,
            '#element_validate' => array('taxonomy_hierarchical_widget_element_validate'),
          );
          
          // Disable select when value is not authorized
          if (!is_null($element_attributes)) {
            $element['depth_' . $i]['#attributes'] += $element_attributes;
            $element_disabled = TRUE;
          }
          else{ // Add ajax callback on the others
            
            // Exept the last one
            if ($i != $taxonomy_tree_max_depth) {
              $element['depth_' . $i]['#ajax'] = array(
                'callback' => 'taxonomy_hierarchical_widget_options_callback',
                'arguments' => array('vocabulary_name' => $field['settings']['allowed_values'][0]['vocabulary']),
              );
            }
          }
        }
    
        // If element not disabled, multiple choice and element not required
        if (!$element_disabled && (isset($instance['widget']['settings']['multiple_choice']) && $instance['widget']['settings']['multiple_choice'] || !$element['#required'])) {
          $element['#attached'] = array('js' => array(drupal_get_path('module', 'taxonomy_hierarchical_widget') . '/js/taxonomy_hierarchical_widget.js'));
          $element['delete'] = array(
            '#title' => t('Delete'),
            '#attributes' => array('class' => array('taxonomy_hierarchical_input_delete_button')),
            '#type' => 'checkbox',
            '#default_value' => FALSE,
            '#element_validate' => array('taxonomy_hierarchical_widget_element_validate'),
          );
        }
      }
    }
    return array('tid' => $element);
  }
}

/**
 * Implements hook form_alter().
 */
function taxonomy_hierarchical_widget_form_alter( &$form, &$form_state, $form_id ) {

  // Only applied on entity forms
  if (isset($form['#entity_type'])) {
    foreach ($form as $field_name => $field_content) {
      if (is_array($field_content) && isset($field_content['#attributes']['class']) && in_array('field-widget-taxonomy-hierarchical-select', $field_content['#attributes']['class'])) {
        $form['#validate'][] = 'taxonomy_hierarchical_widget_form_validate';
        unset($form['workbench_access']['workbench_access']['#required']);
        $form['workbench_access']['#access'] = FALSE;
        
        // Delete automatic added row on edition
        (isset($form[$field_name]['#language'])) ? $field_language = $form[$field_name]['#language'] : $field_language = LANGUAGE_NONE;
        if ((isset($form['#node']) && is_object($form['#node']) && isset($form['#node']->nid)) || (isset($form[$field_name][$field_language]['#max_delta']) && !isset($form['#node']->nid) && $form[$field_name][$field_language]['#max_delta'] > 0)) { 
          if (isset($form[$field_name][$field_language]['#max_delta'])) {
            if (!(isset($form['#node']->nid) && isset($form['#after_build']) && in_array('taxonomy_hierarchical_widget_widget_after_build', $form['#after_build']) && $form[$field_name][$field_language]['#max_delta'] == 0)) {
              $field_max_delta = $form[$field_name][$field_language]['#max_delta'];
              unset($form[$field_name][$field_language][$field_max_delta]);
              $form[$field_name][$field_language]['#max_delta']--;
            }
          }
        }
      }
    }
  }
}

/**
 * Implements method after_build().
 */
function taxonomy_hierarchical_widget_widget_after_build( $form, &$form_state ) {
  
  // Delete add button when field is not multiple
  if (isset($form['#entity_type']) && isset($form['#bundle'])) {
    foreach ($form as $field_name => $field_content) {
      if (is_array($field_content) && isset($field_content['#attributes']['class']) && in_array('field-widget-taxonomy-hierarchical-select', $field_content['#attributes']['class'])) {
        (isset($form[$field_name]['#language'])) ? $language = $form[$field_name]['#language'] : $language = LANGUAGE_NONE;
        unset($form[$field_name][$language]['add_more']);
        unset($form[$field_name][$language][0]['_weight']);
      }
    }
  }
  return $form;
}

/**
 * Implements method element_validate().
 */
function taxonomy_hierarchical_widget_element_validate( $element, &$form_state, $form) {
   
  // Validate each element
  if (isset($element['#value']) && $element['#value'] != '_none') {
    $value = $element['#value'];
    
    // Get array keys
    foreach ($element['#parents'] as $key => $parent) {
      if (preg_match('/depth_([0-9]+)/', $parent))
        $depth_key = $key;
      if (preg_match('/delete/', $parent))
        $delete_key = $key;
    }
    
    // Delete generated 'depth_' parent
    if (isset($depth_key) && $depth_key >= 0)
      unset($element['#parents'][$depth_key]);

    // Delete value
    if (isset($delete_key)) {
      unset($element['#parents'][$delete_key]);
      form_set_value( $element, '', $form_state );
      return;
    }
    
    // Control parent
    $data = $form_state['values'];
    foreach ($element['#parents'] as $parent)
      $data = $data[$parent];
    
    if (is_string($data)) {
      $parents = taxonomy_get_parents( $value );
      if (!isset($parents[$data]))
        return;
    }

    // Update form value
    form_set_value( $element, $value, $form_state );
  }
}

/**
 * Implements method form_validate().
 */
function taxonomy_hierarchical_widget_form_validate( &$form, &$form_state ) {
  global $user;
  
  // Workbench access tree of current user
  $workbench_acces_tree = workbench_access_get_user_tree( $user );
  
  // Run through
  foreach ($form as $field_name => $field_content) {
    if (is_array($field_content) && isset($field_content['#attributes']['class']) && in_array('field-widget-taxonomy-hierarchical-select', $field_content['#attributes']['class'])) {
      (isset($form[$field_name]['#language'])) ? $field_language = $form[$field_name]['#language'] : $field_language = LANGUAGE_NONE;
      (isset($form[$field_name][$field_language]['#title'])) ? $field_title = $form[$field_name][$field_language]['#title'] : $field_title = 'taxonomy reference';
      if (isset($form_state['values']['workbench_access'])) {
        unset($form_state['values']['workbench_access']);
        $values = array();
        
        // Delete add_more value
        if (isset($form_state['values'][$field_name][$field_language]['add_more']))
          unset($form_state['values'][$field_name][$field_language]['add_more']);
        
        // Set error if field is required and no value is selected
        if ((count($form_state['values'][$field_name][$field_language]) == 0 || count($form_state['values'][$field_name][$field_language]) == 1 && $form_state['values'][$field_name][$field_language][0]['tid'] == '') && isset($form[$field_name][$field_language]['#required']) && $form[$field_name][$field_language]['#required'])
          form_set_error($field_name, t('You have to select at least one term for @field_title', array('@field_title' => $field_title)));
        
        // Run through selected values
        foreach ($form_state['values'][$field_name][$field_language] as $key => $value) {
          
          // If value exists, and user have permission to select this term
          if (isset($value['tid']) && is_string($value['tid']) && isset($workbench_acces_tree[$value['tid']])) {
            if (in_array($value['tid'], $values) && (!isset($form_state['triggering_element']['#ajax']['callback']) || $form_state['triggering_element']['#ajax']['callback'] != 'taxonomy_hierarchical_widget_options_callback')) { // Delete duplicate entry
              unset($form_state['values'][$field_name][$field_language][$key]);
              unset($form[$field_name][$field_language][$key]);
            }
            else{ // Add value to the workbench_access field 
              $form_state['values']['workbench_access'][$value['tid']] = $value['tid'];
              $values[$value['tid']] = $value['tid'];
            }
          }
        }
        
        // Set error if none value is authorized
        if (count($values) == 0 && isset($form[$field_name][$field_language]['#required']) && $form[$field_name][$field_language]['#required'])
          form_set_error($field_name, t('Invalid values for @field_title', array('@field_title' => $field_title)));
        
        // Set values to workbench_access element
        $form['workbench_access']['workbench_access']['#default_value'] = $values;
        $form['workbench_access']['workbench_access']['#value'] = $values;
      }
    }
  }
}

/**
 * Implements callback method.
 */
function taxonomy_hierarchical_widget_options_callback( $form, $form_state ) {
  
  foreach ($form as $field_name => $field) {
    if (is_array($field) && isset($field['#attributes']['class']) && in_array('field-widget-taxonomy-hierarchical-select', $field['#attributes']['class'])) {
      $div_classes = '';
      foreach ($field['#attributes']['class'] as $class)
        $div_classes .= '.' . $class;
      $div_replace[] = array('name' =>  $field_name, 'class' => $div_classes);
    }
  }
  
  $ajax = array('#type' => 'ajax');
  foreach ($div_replace as $command_replace)
    $ajax['#commands'][] =  ajax_command_html( $command_replace['class'], render($form[$command_replace['name']]) );
    
  $invoked_ajax = module_invoke_all( 'taxonomy_hierarchical_ajax_command', $form, $form_state );
  $ajax['#commands'] = array_merge( $ajax['#commands'], $invoked_ajax );
  return $ajax;
}


/*********************/
/**     Tools       **/
/*********************/

/**
 * Get vocabulary from the machine_name
 * @param String  $vocabulary_machine_name  Vocabulary machine name
 * @return  Object  Instance of the vocabulary (null if not find)
 */
function taxonomy_hierarchical_widget_get_vocabulary_by_name( $vocabulary_machine_name ) {
  $vocabularies = taxonomy_get_vocabularies();
  foreach ($vocabularies as $vocabulary) {
    if ($vocabulary->machine_name == $vocabulary_machine_name)
      return $vocabulary;
  }
  return NULL;
}

/**
 * Get taxonomy max depth
 * @param Array $taxonomy_tree  Array of objects of type term
 * @return Int  Max depth of the tree 
 */
function taxonomy_hierarchical_widget_get_max_depth( $taxonomy_tree ) {
  $max_depth = 0;
  foreach ($taxonomy_tree as $taxonomy) {
    if ($taxonomy->depth > $max_depth)
      $max_depth = $taxonomy->depth;
  }
  return $max_depth;
}

/**
 * Recursive method to get taxonomy options by depth (including parents of autorised options)
 * @global $user
 * @param Array $taxonomy_tree Array of objects of type term
 * @param Int   $depth         Depth of the wanted terms
 * @param Array of options 
 * @return Array of options with taxonomy of depth = $depth
 */
function taxonomy_hierarchical_widget_get_taxonomy_options( $taxonomy_tree, $depth = NULL, $options = array() ) {
  global $user;
  
  // Default value for depth is max depth of the tree if undefined
  if (is_null($depth))
    $depth = taxonomy_hierarchical_widget_get_max_depth( $taxonomy_tree );
  
  // Workbench access tree of current user
  $workbench_acces_tree = workbench_access_get_user_tree( $user );
  
  // Direct permissions
  foreach ($taxonomy_tree as $taxonomy) {
    if ($taxonomy->depth == $depth && isset($workbench_acces_tree[$taxonomy->tid]))
      $options[$depth][$taxonomy->tid] = $taxonomy->name;
  }
  
  // Indirect permissions (from deepest term)
  if (isset($options[$depth+1])) {
    
    // Add _none option when there is direct rights for parents
    if (isset($options[$depth]))
      $options[$depth+1]['_none'] = t('- None -');
    
    // Add parents of deepest term
    foreach ($options[$depth+1] as $tid => $taxonomy_name) {
      $deepest_taxonomy = taxonomy_get_parents( $tid );
      foreach ($deepest_taxonomy as $parent) {
        if (is_object($parent) && !isset($options[$depth][$parent->tid]))
          $options[$depth][$parent->tid] = $parent->name;
      }
    }
  }
  
  // Recurssive call
  if ($depth > 0)
    $options = taxonomy_hierarchical_widget_get_taxonomy_options( $taxonomy_tree, $depth-1, $options );
  
  return $options;
}

/**
 * Method to generate options select by using parent id
 * @global $user 
 * @param Int $taxonomy_tid Id of the parent taxonomy term
 * @return Array of options
 */
function taxonomy_hierarchical_widget_options_by_parent( $taxonomy_tid ) {
  global $user;
  
  // If parent is none, we force the next options
  if ($taxonomy_tid == '_none') {
    $options['_none'] = t('- None -');
    return $options;
  }
  
  // Adding option _none when user have permission on parent
  $workbench_acces_tree = workbench_access_get_user_tree( $user );
  $options = array();
  if (isset($workbench_acces_tree[$taxonomy_tid]))
    $options['_none'] = t('- None -');
  
  // Run through childrens
  $childrens = taxonomy_get_children( $taxonomy_tid );
  foreach ($childrens as $children) {
    if (isset($workbench_acces_tree[$children->tid])) {
      $options[$children->tid] = $children->name;
    }
    else{ // Indirect permissions
      $grand_childrens = taxonomy_get_children( $children->tid );
      while (count($grand_childrens) > 0) { // Run until there isn't more
        foreach ($grand_childrens as $grand_children) {
          if (isset($workbench_acces_tree[$grand_children->tid])) {
            $options[$children->tid] = $children->name;
          }
        }
        $grand_childrens = taxonomy_get_children( $grand_children->tid );
      }
    }
  }
  return $options;
}

/**
 * Return the default value of select using the selected taxonomy term
 * @param Array $taxonomy_tree  Array of objects of type term
 * @param Array $options  &Options of the select element
 * @param Int $depth  Depth of the select
 * @param Int $taxonomy_id  Id of the taxonomy term
 * @return  String  Default value of the select
 */
function taxonomy_hierarchical_widget_get_default_value( $taxonomy_tree, $options, $depth, $taxonomy_id = NULL ) {
  
  // If taxonomy is undefined
  if (is_null($taxonomy_id)) {
    $default_value = each($options);
    return $default_value['key'];
  }
    
  // Get the taxonomy term (can't use taxonomy_term_load because we need the depth of the term)
  foreach ($taxonomy_tree as $taxonomy) {
    if ($taxonomy->tid == $taxonomy_id) {
      $taxonomy_item = $taxonomy;
      break;
    }
  }
  
  // If taxonomy item was found
  if (isset($taxonomy_item) && $taxonomy_item->depth >= $depth ) {
    if ($depth == $taxonomy_item->depth) {
      return $taxonomy_item->tid;
    }
    elseif ($taxonomy_item->depth > $depth) {
      while (isset($taxonomy_item) && $depth < $taxonomy_item->depth) {
        $parents = taxonomy_get_parents($taxonomy_item->tid);
        foreach ($parents as $parent_id => $parent_taxonomy) {
          foreach ($taxonomy_tree as $taxonomy) {
            if ($taxonomy->tid == $parent_id) {
              $taxonomy_item = $taxonomy;
              break;
            }
            unset($taxonomy_item);
          }

          if (isset($taxonomy_item) && $taxonomy_item->depth == $depth)
            return $taxonomy_item->tid;
        }
      }
    }
  }
  else{
    $default_value = each($options);
    return $default_value['key'];
  }
}

/**
 * Method to get the term instance in the taxonomy tree with the id
 * @param Int $taxonomy_id
 * @return Object Instance of the term (null if don't find)
 */
function taxonomy_hierarchical_widget_get_term_instance( $taxonomy_tree, $taxonomy_id = NULL ) {
  
  // Control taxonomy_id value
  if (!is_null($taxonomy_id)) {
    foreach ($taxonomy_tree as $taxonomy) {
      if ($taxonomy->tid == $taxonomy_id)
        return $taxonomy;
    }
  }
  return NULL;
} 

/**
 * Recurssive method to get the term instance of the parent of a taxonomy
 * @param Int $taxonomy_id
 * @return Object Instance of the term (null if don't find)
 */
function taxonomy_hierarchical_widget_get_depth_parent( $taxonomy_tree, $taxonomy_id, $depth ) {
  $term = taxonomy_hierarchical_widget_get_term_instance( $taxonomy_tree, $taxonomy_id );
  if ($term->depth == $depth)
    return $term;
  
  foreach ($term->parents as $parent_id)
    return taxonomy_hierarchical_widget_get_depth_parent( $taxonomy_tree, $parent_id, $depth);
}