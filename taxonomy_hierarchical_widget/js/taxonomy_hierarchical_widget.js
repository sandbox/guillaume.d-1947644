/**
* @file
* Javascript
*/
(function($) {

  $(function(){

    Drupal.behaviors.taxonomyHierarchicalWidget = {
      attach: function(context, settings){
        $('.field-type-taxonomy-term-reference.field-widget-taxonomy-hierarchical-select.form-wrapper table.field-multiple-table tbody tr').once('js-thw-delete').each(function(){
          var $row = $(this);
          var $check = $('.form-checkbox[name*="tid"][name*="delete"]', $row);
          var $button = $('<a class="button" href="#" />').text(Drupal.t('Delete'));

          if( $check.is(':checked') ) {
            /* hide already checked rows */
            $row.addClass('deleted');
          } else {
            $button.appendTo($check.parent()).click(function(e){
              e.preventDefault();
              $check.attr('checked', 'checked');
              $check.parents('td:first').find('select:first').trigger('change');
              $row.fadeOut();
            });
          }
        });
      }
    };
    Drupal.attachBehaviors();
  });
})(jQuery);
